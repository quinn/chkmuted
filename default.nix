{ stdenv, rustPlatform }:

rustPlatform.buildRustPackage rec {
  pname = "chkmuted";
  version = "0.0.1";

  src = ./.;

  cargoSha256 = "zOjOhPF1khRkfeHx8jBC2L6mwj6/aX4wNlMUJJKSO6k=";

  meta = with stdenv.lib; {
    description = "Check if muted using ALSA";
    license = licenses.gpl3;
  };
}

{
  description = "Check if muted using ALSA";

  outputs = { self, nixpkgs }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
      chkmuted = pkgs.callPackage ./. {};
    in
      {
        packages.x86_64-linux.chkmuted = chkmuted;
        defaultPackage.x86_64-linux = self.packages.x86_64-linux.chkmuted;
      };
}

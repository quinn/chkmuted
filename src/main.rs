use std::process::Command;
use std::io;
// use the bytes version to avoid converting to utf-8
// since we know the amixer output doesn't use it
use regex::bytes::Regex;
use lazy_static::lazy_static;

const OUTPUT: &str = "";

// This whole program could probably also be written using the alsa-lib
// package, and avoid calling amixer altogether. But I didn't have the time
// and energy to implement that.

fn get_amixer_status() -> io::Result<bool>  {
    lazy_static! {
        static ref AMIXER_REGEX_OFF: regex::bytes::Regex =
            Regex::new(": values=off")
            .unwrap();
        static ref AMIXER_REGEX_ON: regex::bytes::Regex =
            Regex::new(": values=on")
            .unwrap();
    }
    let amixer_output = Command::new("amixer")
        .args(&["cget", "numid=2,iface=MIXER,name='Capture Switch'"])
        .output()?
        .stdout;
    let capture_on = AMIXER_REGEX_ON.is_match(&amixer_output);
    let capture_off = AMIXER_REGEX_OFF.is_match(&amixer_output);
    if capture_on == capture_off {
        Err(io::Error::new(io::ErrorKind::InvalidData,
                           "Could not determine amixer capture status"))
    } else {
        Ok(capture_on)
    }
}

fn main() {
    let s = get_amixer_status().unwrap();
    if s {
        println!("{}", OUTPUT);
    } else {
        println!("");
    }
}
